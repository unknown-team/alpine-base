docker_args = --compress -t unknown/base:3.7 .

build: Dockerfile container/drop wait-for/wait-for
	sudo docker build ${docker_args} \
		--network none
