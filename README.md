# Base Container
- A wrapper around su-exec (```drop```)

## Dropping Privilege
Installed at ```/usr/local/bin/drop```

Configuration (via environment variables):

- ```USERID```: The UID of the dropped-to user (default: 9001)
- ```GROUPID```: The GID of the dropped-to group (default: 9001)
- ```CUSER```: The username of the dropped-to user
- ```UMASK```: The umask that is set (default: 077)

If the group or the user already exist, then the wrapper won't try to
re-create them.
