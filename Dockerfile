FROM alpine:3.7
LABEL maintainer Alexander 'Polynomdivision'

# Install a way of dropping privilege
RUN apk add --no-cache su-exec

# Add a wrapper around su-exec
ADD container/drop /usr/local/bin/drop
RUN chmod 500 /usr/local/bin/drop

ADD wait-for/wait-for /usr/local/bin/wait-for
RUN chmod 555 /usr/local/bin/wait-for
